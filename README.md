### [![Repository on GitLab](https://img.shields.io/badge/Repository-here%20on%20GitLab-success?style=for-the-badge&logo=arch-linux)](https://nobodyinperson.gitlab.io/repository)  :arrow_left: :arrow_left: click here for setup instructions

# Arch Linux / Manjaro Repository

This project is a collection of [Arch Linux](https://archlinux.org) / [Manjaro](https://manjaro.org) [PKGBUILD](https://wiki.archlinux.org/index.php/PKGBUILD)s for building Arch Linux packages.

The packages in this project are supposed to ease the setup and maintenance of my Arch Linux-based machines.

In the [Pipelines](https://gitlab.com/nobodyinperson/repository/-/pipelines), all packages in this project are built for each new commit. On the `master`-branch, these packages are then deployed to [this project's GitLab Pages site](https://nobodyinperson.gitlab.io/repository), where you can find instructions how to use this repository on your Arch Linux / Manjaro machine.
